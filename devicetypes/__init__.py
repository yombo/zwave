from collections import defaultdict
import copy
from time import time

# Import twisted libraries
# from twisted.internet.defer import inlineCallbacks
# from twisted.internet.task import LoopingCall
from twisted.internet import reactor

from yombo.constants.status_extra import STATUS_EXTRA_BATTERY_LEVEL, STATUS_EXTRA_ORIENTATION
from yombo.core.log import get_logger
from yombo.utils.decorators import memoize_ttl
from yombo.utils import dict_find_key

logger = get_logger("modules.zwave.devices")

AEOTEC = 0x86
AEOTEC_ZW098_LED_BULB = 0x62
AEOTEC_ZW098_LED_BULB_LIGHT = (AEOTEC, AEOTEC_ZW098_LED_BULB)

COLOR_CHANNEL_WARM_WHITE = 0x01
COLOR_CHANNEL_COLD_WHITE = 0x02
COLOR_CHANNEL_RED = 0x04
COLOR_CHANNEL_GREEN = 0x08
COLOR_CHANNEL_BLUE = 0x10

# Generate midpoint color temperatures for bulbs that have limited
# support for white light colors
TEMP_COLOR_MAX = 500  # mireds (inverted)
TEMP_COLOR_MIN = 154
TEMP_MID_YOMBO = (TEMP_COLOR_MAX - TEMP_COLOR_MIN) / 2 + TEMP_COLOR_MIN
TEMP_WARM_YOMBO = (TEMP_COLOR_MAX - TEMP_COLOR_MIN) / 3 * 2 + TEMP_COLOR_MIN
TEMP_COLD_YOMBO = (TEMP_COLOR_MAX - TEMP_COLOR_MIN) / 3 + TEMP_COLOR_MIN

WORKAROUND_ZW098 = 'zw098'

DEVICE_MAPPINGS = {
    AEOTEC_ZW098_LED_BULB_LIGHT: WORKAROUND_ZW098
}


class Zwave_Node(object):
    """
    This is a skeleton class represents a z-wave node (a switch, light, lock, alarm, sensor, etc)
    """
    FRIENDLY_LABEL = "Zwave Device"

    @property
    def status(self):
        return None

    @property
    def status_extra(self):
        return {}

    def __init__(self, parent, network, node, schema, primary_value):
        """
        Initialize a new ZwaveNode object.

        @param network: the home_id of the node
        @param node: the node_id of the node
        """
        self.commands = {}
        self._Parent = parent
        self.device_type = self._Parent._DeviceTypes.get('zwave_generic')
        self.device_commands = parent._Devices.device_commands
        self.node = node
        self.values = node.values
        self.network = network
        self.last_update = None
        self.yombo_device = None

        self.last_command = None
        self.last_request_id = None
        self.has_battery = False

        self.values_as_status_extra = []
        self.machine_status_extra = None
        self.VALUE_LABEL_TO_STATUS_EXTRA = {
            STATUS_EXTRA_BATTERY_LEVEL: "Battery Level",
            STATUS_EXTRA_ORIENTATION: "Orientation",
        }

        # self._schema = copy.deepcopy(schema)
        # for name in self._schema['values'].keys():
        #     self.values[name] = None
        #     self._schema['values'][name]['instance'] = [primary_value.instance]

        self.primary_value_slug = primary_value.label
        self.primary_value_id = primary_value.value_id

    def attach_yombo_device(self, yombo_device):
        """
        Attach a yombo device. This allows status updates and control commands to be used.

        :param yombo_device:
        :return: 
        """
        self.yombo_device = yombo_device
        self.machine_status_extra = self.yombo_device.machine_status_extra
        device_variables = yombo_device.device_variables_cached
        if 'poll_intensity' in device_variables:
            poll_intensity = device_variables['poll_intensity']['values'][0]
        else:
            poll_intensity = 10

        self.has_battery = self.node.get_battery_level() is not None

        if poll_intensity > 0 and self.node.get_battery_level() is None:
            self.primary_value.enable_poll(poll_intensity)
        else:
            self.primary_value.disable_poll()

        yombo_device.zwave_setup(self)

        # Now sync zwave device to yombo device status
        for value_id, value in self.node.values.items():
            self.update_value(value)

    # def add_node_value(self, value):
    #     # print("Node: %s ... add_node_value: %s" % (self.node_id, value))
    #     if value.value_id in self.value_ids:
    #         slug = self.value_ids[value.value_id]
    #     else:
    #         slug = self._Parent._Validate.slugify(value.label)
    #     self.value_ids[value.value_id] = slug
    #     self.values[slug] = value

    def find_value(self, label):
        return dict_find_key(self.VALUE_LABEL_TO_STATUS_EXTRA, label)

    def update_value(self, value):
        if self.yombo_device is None:
            logger.debug("ZWave got a status update for a device that has no matching Yombo device. Discarding for node: {node_id}", node_id=self.node_id)
            return

        label = value.label
        new = value.data
        try:
            status_extra_label = self.find_value(label)
        except IndexError:
            logger.debug("Received a value update that is not tracked as a status extra")
            return

        try:
            old = self.yombo_device.get_status_extra(status_extra_label)
        except KeyError:
            old = "I wasn't found, but i'm also not none. So, I'm something, but not quite sure what. &YQJ-Gp&nw!V=@tM)36!9Z"

        if old == new:  # Stale value.  Bye bye
            return

        # We try to guess if the status update was a result of a command request.
        last_device_command = None

        if self.last_request_id is not None and self.last_request_id in self.device_commands:
            last_device_command = self.device_commands[self.last_request_id]
            if time() - last_device_command.broadcast_at > 2:
                self.last_request_id = None
            else:
                self.last_command = last_device_command.command

        self.update_status(status_extra_label, value, last_device_command)

    def update_status(self, status_extra_label, value, last_device_command):
        pass

    def set_status(self, status, status_extra, last_command=None, last_device_command=None, delay=None):
        if delay is None:
            battery_level = self.node.get_battery_level()
            if battery_level is None:
                delay = 0.100
            else:
                delay = 0.300

        if last_device_command is not None:
            command = last_device_command.command
            request_id = last_device_command.request_id
        else:
            command = None
            request_id = None

        if last_command is not None:
            command = last_command

        if status is None:
            self.yombo_device.set_status_delayed(
                delay=delay,
                machine_status_extra=status_extra,
                request_id=request_id,
                reported_by="Zwave node"
            )
        else:
            self.yombo_device.set_status_delayed(
                delay=delay,
                command=command,
                request_id=request_id,
                machine_status=status,
                machine_status_extra=status_extra,
                reported_by="Zwave node"
            )

    @property
    def home_id(self):
        return self.network.home_id

    @property
    def node_id(self):
        return self.node.node_id

    @property
    def manufacturer(self):
        return self.node.manufacturer_name

    @property
    def primary_value(self):
        return self.values[self.primary_value_id]

    @property
    def brightness(self):
        value = self.get_value_by_label('Level')
        return int(value.data)

    @property
    def rgb_color(self):
        return self._rgb

    @property
    def value_labels(self):
        values = []
        for value_id, value in self.node.values.items():
            values.append(value.label)
        return values

    @property
    @memoize_ttl(30)
    def values_by_label(self):
        values = {}
        for value_id, value in self.node.values.items():
            values[value.label] = value
        return values

    def get_value_by_label(self, label):
        for value_id, value in self.node.values.items():
            if value.label == label:
                return value
        raise KeyError("Label not found in node values: %s" % label)

    def add_value(self, value):
        """
        Adds a value to this node
        @param value: the value to add to the node
        """
        print("ADD_VALUE!")
        self.values.append(value)
        self.fix_labels()

    def fix_labels(self):
        """
        Makes sure this node has no duplicate value labels
        """
        labels = []
        duplicates = []
        for value in self.values:
            if labels.count(value.value_data["label"]) == 0:
                labels.append(value.value_data["label"])
            elif duplicates.count(value.value_data["label"]) == 0:
                duplicates.append(value.value_data["label"])

        for label in duplicates:

            for value in self.values:
                if value.value_data["label"] == label:
                    instance = value.value_data["instance"]
                    index = value.value_data["index"]
                    cclass = value.value_data["commandClass"]
                    break

            instanceDifferent = False
            indexDifferent = False
            cclassDifferent = False

            for value in self.values:
                if value.value_data["label"] == label:
                    if instance != value.value_data["instance"]:
                        instanceDifferent = True
                    if index != value.value_data["index"]:
                        indexDifferent = True
                    if cclass != value.value_data["commandClass"]:
                        cclassDifferent = True

            for value in self.values:
                if value.value_data["label"] == label:
                    value.label = value.value_data["label"] + " "
                    if instanceDifferent:
                        value.label += "#" + str(value.value_data["instance"])
                    if indexDifferent:
                        value.label += "/" + str(value.value_data["index"])
                    if cclassDifferent:
                        value.label += "@" + str(value.value_data["commandClass"]).replace("COMMAND_CLASS_", "")

    def __str__(self):
        return 'home_id: [{0}]  node_id: [{1}]  manufacturer: [{2}] product: [{3}]'.format(self.home_id,
                                                                                           self.node_id,
                                                                                           self.node.manufacturer_name,
                                                                                           self.node.product_name)

    def do_command(self, **kwargs):
        command = kwargs['command']
        if command.machine_label in self.commands:
            if command.machine_label not in self.commands:
                return 'failed', _('module.zwave',
                                   "Cannot find the required command method: %s" % command.machine_label)
            self.last_request_id = kwargs['request_id']
            try:
                self.commands[command.machine_label](**kwargs)
                return 'done', 'Command delivered to Zwave.'
            except Exception as e:
                return 'failed', _('module.zwave',
                                   "Error processing request: %s" % e)
        else:
            logger.warn("ZWave cannot handle command '{command}' for device '{device}'",
                        command=command.label, device=kwargs['device'].label)
            return 'failed', _('module.zwave', "Cannot find the required command method.")

    def turn_on(self, **kwargs):
        return 'fail', "Generic Zwave processing unable to perform command."

    def turn_off(self, **kwargs):
        return 'fail', "Generic Zwave processing unable to perform command."
