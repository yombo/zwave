import time

from . import Zwave_Node
from .. import workaround

from yombo.core.log import get_logger

logger = get_logger("modules.zwave.devicetypes.sensors")

class ZWave_Sensor(Zwave_Node):
    """Representation of a Z-Wave sensor."""

    FRIENDLY_LABEL = "Zwave Sensor"

    def __init__(self, parent, network, node, schema, primary_value):
        """Initialize the light."""
        Zwave_Node.__init__(self, parent, network, node, schema, primary_value)
        self.device_type = self._Parent._DeviceTypes.get('zwave_sensor')
        self._state = self.primary_value.data
        self._units = self.primary_value.units

    def update_status(self, value, last_request):
        new = value.data
        if self.last_command is None:
            new = int(new)
            if new == 0:
                last_command_label = 'unlock'
            else:
                last_command_label = 'lock'
            self.last_command = self._Parent._Commands[last_command_label]

        self.status = new
        self._units = value.units
        self.status_extra['units'] = self._units


class ZWave_Multilevel_Sensor(ZWave_Sensor):
    """Representation of a multi level sensor Z-Wave sensor."""

    FRIENDLY_LABEL = "Zwave Multi Level Sensor"

    def __init__(self, parent, network, node, schema, primary_value):
        """Initialize the light."""
        Zwave_Node.__init__(self, parent, network, node, schema, primary_value)
        self.device_type = self._Parent._DeviceTypes.get('zwave_multilevel_sensor')


class ZWave_Alarm_Sensor(ZWave_Sensor):
    """Representation of a Z-Wave sensor that sends Alarm alerts.

    Examples include certain Multisensors that have motion and vibration
    capabilities. Z-Wave defines various alarm types such as Smoke, Flood,
    Burglar, CarbonMonoxide, etc.

    This wraps these alarms and allows you to use them to trigger things, etc.

    COMMAND_CLASS_ALARM is what we get here.
    """

    FRIENDLY_LABEL = "Zwave Alarm Sensor"

class ZWave_Binary_Sensor(ZWave_Sensor):
    """Representation of a binary sensor within Z-Wave."""

    def __init__(self, values, device_class):
        """Initialize the sensor."""
        zwave.ZWaveDeviceEntity.__init__(self, values, PLATFORM_ZWAVE)
        self._sensor_type = device_class
        self._state = self.values.primary.data

    def update_properties(self):
        """Handle data changes for node values."""
        self._state = self.values.primary.data

    @property
    def is_on(self):
        """Return true if the binary sensor is on."""
        return self._state

    @property
    def device_class(self):
        """Return the class of this sensor, from DEVICE_CLASSES."""
        return self._sensor_type


class ZWaveTriggerSensor(ZWave_Sensor):
    """Representation of a stateless sensor within Z-Wave."""

    def __init__(self, values, device_class):
        """Initialize the sensor."""
        super(ZWaveTriggerSensor, self).__init__(values, device_class)
        # Set default off delay to 60 sec
        self.re_arm_sec = 60
        self.invalidate_after = None

    def update_properties(self):
        """Handle value changes for this entity's node."""
        self._state = self.values.primary.data
        _LOGGER.debug('off_delay=%s', self.values.off_delay)
        # Set re_arm_sec if off_delay is provided from the sensor
        if self.values.off_delay:
            _LOGGER.debug('off_delay.data=%s', self.values.off_delay.data)
            self.re_arm_sec = self.values.off_delay.data * 8
        # only allow this value to be true for re_arm secs
        if not self.hass:
            return

        self.invalidate_after = dt_util.utcnow() + datetime.timedelta(
            seconds=self.re_arm_sec)
        track_point_in_time(
            self.hass, self.async_update_ha_state,
            self.invalidate_after)

    @property
    def is_on(self):
        """Return true if movement has happened within the rearm time."""
        return self._state and \
            (self.invalidate_after is None or
             self.invalidate_after > dt_util.utcnow())