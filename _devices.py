"""
This file is used by the Yombo core to create a device object for the specific zwave devices.
"""
import re

from yombo.lib.devices.fan import Fan
from yombo.lib.devices.lock import Lock
from yombo.lib.devices.light import Light, Color_Light
from yombo.lib.devices.switch import Switch
from yombo.constants.features import (FEATURE_BRIGHTNESS, FEATURE_SEND_UPDATES, FEATURE_EFFECT, FEATURE_PERCENT,
    FEATURE_RGB_COLOR, FEATURE_TRANSITION, FEATURE_WHITE_VALUE, FEATURE_XY_COLOR, FEATURE_NUMBER_OF_STEPS,
    FEATURE_COLOR_TEMP, FEATURE_SUPPORT_COLOR, FEATURE_BATTERY_POWERED)
from yombo.constants.status_extra import STATUS_EXTRA_BRIGHTNESS
from yombo.utils import translate_int_value

STATUS_HOLDS_AUTO_AWAY = 'auto_away'

COLOR_CHANNEL_WARM_WHITE = 0x01
COLOR_CHANNEL_COLD_WHITE = 0x02
COLOR_CHANNEL_RED = 0x04
COLOR_CHANNEL_GREEN = 0x08
COLOR_CHANNEL_BLUE = 0x10


class ZWave_Device(object):

    @property
    def brightness(self):
        """
        Return the brightness as a percent for this light. Returns a range between 0 and 100, converts based on the
        'number_of_steps'.
        """
        if self.zwave_device is None:
            return 0

        return self.zwave_device.brightness

    @property
    def percent(self):
        """
        Return the brightness as a percent for this light. Returns a range between 0 and 100, converts based on the
        'number_of_steps'.
        """
        if self.zwave_device is None:
            return 0

        brightness = self.zwave_device.brightness
        return translate_int_value(brightness,
                                   0, self.FEATURES[FEATURE_NUMBER_OF_STEPS],
                                   0, 100)

    @property
    def debug_data(self):
        """
        Provide zwave node values to the debug display.

        :return:
        """
        debug_data = super().debug_data
        if hasattr(self, 'zwave_device'):
            data_final = {}
            for value_id, value in self.zwave_device.node.values.items():
                name = value.label
                data = value.data
                has_match = re.search(r'\d+$', name[:-1]) is not None
                value_final = {}
                if name.startswith('Code ') and has_match:
                    value_final['value'] = "*************"
                else:
                    if data is None:
                        value_final['value'] = "NULL"
                    else:
                        value_final['value'] = value.data
                if value is not None:
                    value_final['genre'] = value.genre
                    value_final['units'] = value.units
                    value_final['type'] = value.type
                    data_final[name] = value_final

            debug_data['zwave'] = {
                'title': 'Zwave Values',
                'description': 'Values for each zwave node, as reported by the node.',
                'fields': ['Value name', 'Value data'],
                'data': data_final,
            }
        else:
            debug_data['zwave'] = {
                'title': 'Zwave Values',
                'description': 'Values for each zwave node, as reported by the node.',
                'fields': ['Value name', 'Value data'],
                'data': {'Zwave module still loading': "Try later."},
            }
        return debug_data

    def zwave_setup(self, zwave_device):
        """
        Attach the zwave node to the Yombo Device instance.

        :param zwave_device:
        :return:
        """
        self.zwave_device = zwave_device
        self.node = zwave_device.node
        self.zvalues = zwave_device.node.values
        self.zvalue_labels = zwave_device.value_labels
        self.zvalues_by_label = zwave_device.values_by_label
        self.zprimary_value = zwave_device.primary_value
        value_labels = self.zvalue_labels  # all possible node value labels.

        if zwave_device.has_battery:
            self.FEATURES[FEATURE_BATTERY_POWERED] = True
            self.set_status_delayed(
                delay=0.5,
                machine_status_extra={'battery_level': zwave_device.node.get_battery_level()})

        for status_extra_label, value_label in zwave_device.VALUE_LABEL_TO_STATUS_EXTRA.items():
            if value_label in value_labels:
                self.MACHINE_STATUS_EXTRA_FIELDS[status_extra_label] = True
                if status_extra_label in self.FEATURES:
                    self.FEATURES[status_extra_label] = True
            else:
                if status_extra_label in self.MACHINE_STATUS_EXTRA_FIELDS:
                    self.MACHINE_STATUS_EXTRA_FIELDS[status_extra_label] = False
                if status_extra_label in self.FEATURES:
                    self.FEATURES[status_extra_label] = False


class ZWave_Fan(ZWave_Device, Fan):
    """
    Simple zwave fan device
    """
    def __init__(self, *args, **kwargs):
        self.zwave_device = None
        super().__init__(*args, **kwargs)
        self.SUB_PLATFORM = 'zwave'
        self.FEATURES.update({
            FEATURE_NUMBER_OF_STEPS: 99,
            FEATURE_SEND_UPDATES: True,
            FEATURE_BRIGHTNESS: True,
            }
        )

    def set_percent(self, percent, **kwargs):
        """
        Other systems may call this, this is simply a wrapper.

        :param args:
        :param kwargs:
        :return:
        """
        if 'inputs' in kwargs:
            kwargs['inputs']['percent'] = percent
        else:
            kwargs['inputs'] = {'percent': percent}

        return self.turn_on(**kwargs)


class ZWave_ColorLight(ZWave_Device, Color_Light):
    """
    Simple zwave light device, non-colorized.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.SUB_PLATFORM = 'zwave'
        self.FEATURES.update({
            FEATURE_NUMBER_OF_STEPS: 99,
            FEATURE_SEND_UPDATES: True,
            FEATURE_SUPPORT_COLOR: True,
            FEATURE_EFFECT: False,
            FEATURE_RGB_COLOR: True,
            FEATURE_XY_COLOR: False,
            FEATURE_COLOR_TEMP: False,
            'dimming_duration': False,
            FEATURE_WHITE_VALUE: False,
            }
        )
        self.MACHINE_STATUS_EXTRA_FIELDS[STATUS_EXTRA_BRIGHTNESS] = False

    @property
    def rgb_color(self):
        """
        Return the RGB color value [int, int, int].
        """
        if self.zwave_device is None:
            return 0, 0, 0

        return self.zwave_device.rgb_color

    # @property
    # def brightness(self):
    #     """
    #     Return the brightness as a percent for this light. Returns a range between 0 and 100, converts based on the
    #     'number_of_steps'.
    #     """
    #     if self.zwave_device is None:
    #         return 0
    #
    #     percent = translate_int_value(self.zwave_device.brightness,
    #                                   0, self.FEATURES[FEATURE_NUMBER_OF_STEPS],
    #                                   0, 100) * 0.01
    #     rgb = self.zwave_device.rgb_color
    #     rgb_brightness = color_util.rgb_to_brighess(rgb[0], rgb[1], rgb[2])
    #     calc_brightness = rgb_brightness * percent
    #     return translate_int_value(calc_brightness,
    #                                0, 255,
    #                                0, self.FEATURES[FEATURE_NUMBER_OF_STEPS])
    #
    # @property
    # def percent(self):
    #     """
    #     Return the brightness as a percent for this light. Returns a range between 0 and 100, converts based on the
    #     'number_of_steps'.
    #     """
    #     if self.zwave_device is None:
    #         return 0
    #
    #     percent = translate_int_value(self.zwave_device.brightness,
    #                                   0, self.FEATURES[FEATURE_NUMBER_OF_STEPS],
    #                                   0, 100) * 0.01
    #     rgb = self.zwave_device.rgb_color
    #     rgb_brightness = color_util.rgb_to_brighess(rgb[0], rgb[1], rgb[2])
    #     calc_brightness = rgb_brightness * percent
    #     return translate_int_value(calc_brightness,
    #                                0, 255,
    #                                0, 100)

    def zwave_setup(self, zwave_device):
        super().zwave_setup(zwave_device)

        if 'Color' in self.zvalues_by_label and self.zvalues_by_label['Color'] is not None:
            if self.zwave_device._zw098:
                self.zwave_device.FEATURES[FEATURE_COLOR_TEMP] = True
            elif self.zwave_device._color_channels is not None and self.zwave_device._color_channels & (
                    COLOR_CHANNEL_WARM_WHITE | COLOR_CHANNEL_COLD_WHITE):
                self.zwave_device.FEATURES[FEATURE_WHITE_VALUE] = True


class ZWave_Light(ZWave_Device, Light):
    """
    Simple zwave light device, non-colorized.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.SUB_PLATFORM = 'zwave'
        self.FEATURES.update({
            FEATURE_NUMBER_OF_STEPS: 99,
            FEATURE_SEND_UPDATES: True,
            }
        )
        self.MACHINE_STATUS_EXTRA_FIELDS[STATUS_EXTRA_BRIGHTNESS] = False


class ZWave_Lock(ZWave_Device, Lock):
    """
    Simple zwave lock device
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.SUB_PLATFORM = 'zwave'
        self.FEATURES.update({
            FEATURE_NUMBER_OF_STEPS: False,
            FEATURE_SEND_UPDATES: True,
            }
        )


class ZWave_Switch(ZWave_Device, Switch):
    """
    Simple zwave Switch device
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.SUB_PLATFORM = 'zwave'
        self.FEATURES.update({
            FEATURE_NUMBER_OF_STEPS: 99,
            FEATURE_SEND_UPDATES: True,
            }
        )
