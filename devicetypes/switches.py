import time

from . import Zwave_Node
from .. import workaround

from yombo.core.log import get_logger

logger = get_logger("modules.zwave.devicetypes.switch")

class Zwave_Switch(Zwave_Node):
    """Representation of a Z-Wave switch."""

    FRIENDLY_LABEL = "Zwave Switch"

    def __init__(self, parent, network, node, schema, primary_value):
        """Initialize the Z-Wave switch device."""
        Zwave_Node.__init__(self, parent, network, node, schema, primary_value)
        self.device_type = self._Parent._DeviceTypes.get('zwave_switch')
        self.commands = {
            'on': self.turn_on,
            'off': self.turn_off,
        }

        self.refresh_on_update = (
            workaround.get_device_mapping(self.primary_value) ==
            workaround.WORKAROUND_REFRESH_NODE_ON_UPDATE)
        self.last_update = time.perf_counter()
        self._state = primary_value
        print("switch state")

    def update_status(self, value, last_request):
        print("UPDATE STATUS!!!!!!!!!!!!!!!!!!!!!!!!! Zwave_Switch: %s" % value)
        new = value.data
        if self.last_command is None:
            new = int(new)
            if new == 0:
                last_command_label = 'off'
                self.status = 0
            else:
                last_command_label = 'on'
                self.status = 1
            self.last_command = self._Parent._Commands[last_command_label]
        else:
            self.status = int(new)


    @property
    def is_on(self):
        """Return true if device is on."""
        return self._state

    def turn_on(self, **kwargs):
        """Turn the device on."""
        self.node.set_switch(self.primary_value.value_id, True)

    def turn_off(self, **kwargs):
        """Turn the device off."""
        self.node.set_switch(self.primary_value.value_id, False)
