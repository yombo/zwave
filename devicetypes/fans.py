import math

from . import Zwave_Node
from .. import const

from yombo.constants.status_extra import STATUS_EXTRA_SPEED, STATUS_EXTRA_SPEED_LABEL
from yombo.constants.devicetypes.fan import *
from yombo.core.log import get_logger

logger = get_logger("modules.zwave.devicetypes.fan")

# Value will first be divided to an integer
VALUE_TO_SPEED = {
    0: SPEED_OFF,
    1: SPEED_LOW,
    2: SPEED_MEDIUM,
    3: SPEED_HIGH,
}

SPEED_TO_VALUE = {
    SPEED_OFF: 0,
    SPEED_LOW: 33,
    SPEED_MEDIUM: 66,
    SPEED_HIGH: 99,
}


class Zwave_Fan(Zwave_Node):
    """Representation of a Z-Wave fan."""

    FRIENDLY_LABEL = "Zwave Fan"

    def __init__(self, parent, network, node, schema, primary_value):
        """Initialize the light."""
        Zwave_Node.__init__(self, parent, network, node, schema, primary_value)
        self.commands = {
            'on': self.turn_on,
            'set_brightness': self.turn_on,
            'set_percent': self.turn_on,
            'set_speed': self.set_speed,
            'off': self.turn_off,
        }
        self.device_type = self._Parent._DeviceTypes.get('zwave_fan')

    def find_speed(self, input_value):
        if input_value == 0:
            return SPEED_OFF
        elif input_value <= 33:
            return SPEED_LOW
        elif input_value <= 66:
            return SPEED_MEDIUM
        elif input_value <= 99:
            return SPEED_HIGH

    def update_status(self, feature, value, last_request):
        print("UPDATE STATUS!!!!!!!!!!!!!!!!!!!!!!!!! Zwave_Fan: %s" % value)
        battery_level = self.node.get_battery_level()
        if battery_level is None:
            delay = 0.1
        else:
            delay = 0.3
        return
        if feature == STATUS_EXTRA_LOCKED:
            self.yombo_device.set_status_delayed(delay=delay, machine_status=is_one_zero(value.data))
        elif feature in (STATUS_EXTRA_BATTERY_LEVEL):
            self.yombo_device.set_status_delayed(delay=delay, machine_status_extra={feature: value.data})


        percentage = int(value.data)
        speed_name = self.find_speed(percentage)
        speed_int = SPEED_TO_VALUE[speed_name]

        if self.last_command is None:
            if speed_int == 0:
                last_command_label = 'off'
            else:
                last_command_label = 'on'
            self.last_command = self._Parent._Commands[last_command_label]
        if speed_int > 0:
            self.status = 0
        else:
            self.status = 1
        self.status_extra[STATUS_EXTRA_SPEED] = speed_int
        self.status_extra[STATUS_EXTRA_SPEED_LABEL] = speed_name

    def set_speed(self, speed):
        """Set the speed of the fan."""
        if speed in SPEED_TO_VALUE:
            self.node.set_dimmer(self.primary_value.value_id, SPEED_TO_VALUE[speed])
        else:
            raise Exception("Speed must be 'off', 'low', 'medium', or 'high'.")

    def turn_on(self, **kwargs):
        """Turn the device on."""
        inputs = kwargs['inputs']
        print("turn_on inputs:: %s" % inputs)
        if 'brightness' in inputs:
            speed = int(inputs['brightness'])
        elif 'percent' in inputs:
            speed = int(inputs['percent'])
        elif 'speed' in inputs:
            return self.set_speed(inputs['speed'])
        else:
            # Value 255 tells device to return to previous value
            speed = 255

        self.node.set_dimmer(self.primary_value.value_id, speed)

    def turn_off(self, **kwargs):
        """Turn the device off."""
        self.node.set_dimmer(self.primary_value.value_id, 0)
