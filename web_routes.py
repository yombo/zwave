try:  # Prefer simplejson if installed, otherwise json will work swell.
    import simplejson as json
except ImportError:
    import json

from twisted.internet.defer import inlineCallbacks

from yombo.lib.webinterface.auth import require_auth
from yombo.core.log import get_logger

logger = get_logger("modules.zwave.web_routes")

def module_zwave_routes(webapp):
    """
    Adds routes to the webinterface module.

    :param webapp: A pointer to the webapp, it's used to setup routes.
    :return:
    """
    with webapp.subroute("/module_settings") as webapp:

        def root_breadcrumb(webinterface, request):
            webinterface.add_breadcrumb(request, "/?", "Home")
            webinterface.add_breadcrumb(request, "/modules/index", "Modules")
            webinterface.add_breadcrumb(request, "/module_settings/zwave/index", "Z-Wave")

        @webapp.route("/zwave", methods=['GET'])
        @require_auth()
        def page_module_zwave_get(webinterface, request, session):
            return webinterface.redirect(request, '/module_settings/zwave/index')

        @webapp.route("/zwave/index", methods=['GET'])
        @require_auth()
        def page_tools_module_zwave_index_get(webinterface, request, session):
            zwave = webinterface._Modules['ZWave']

            page = webinterface.webapp.templates.get_template('modules/zwave/web/index.html')
            root_breadcrumb(webinterface, request)
            return page.render(alerts=webinterface.get_alerts(),
                               )

        @webapp.route("/zwave/heal", methods=['GET'])
        @require_auth()
        def page_tools_module_zwave_heal_get(webinterface, request, session):
            zwave = webinterface._Modules['ZWave']

            zwave.heal()
            page = webinterface.webapp.templates.get_template('modules/zwave/web/heal.html')
            root_breadcrumb(webinterface, request)
            webinterface.add_breadcrumb(request, "/module_settings/zwave/heal", "Heal")
            return page.render(alerts=webinterface.get_alerts())

        @webapp.route("/zwave/add_node", methods=['GET'])
        @require_auth()
        def page_tools_module_zwave_add_node_get(webinterface, request, session):
            zwave = webinterface._Modules['ZWave']

            zwave.add_node()
            page = webinterface.webapp.templates.get_template('modules/zwave/web/add_node.html')
            root_breadcrumb(webinterface, request)
            webinterface.add_breadcrumb(request, "/module_settings/zwave/add_node", "Add Node")
            return page.render(alerts=webinterface.get_alerts())


        @webapp.route("/zwave/add_node_secure", methods=['GET'])
        @require_auth()
        def page_tools_module_zwave_add_node_secure_get(webinterface, request, session):
            zwave = webinterface._Modules['ZWave']

            zwave.add_node_secure()
            page = webinterface.webapp.templates.get_template('modules/zwave/web/add_node_secure.html')
            root_breadcrumb(webinterface, request)
            webinterface.add_breadcrumb(request, "/module_settings/zwave/add_node_secure", "Add Node Secure")
            return page.render(alerts=webinterface.get_alerts())

        @webapp.route("/zwave/remove_node", methods=['GET'])
        @require_auth()
        def page_tools_module_zwave_remove_node_get(webinterface, request, session):
            zwave = webinterface._Modules['ZWave']

            zwave.remove_node()
            page = webinterface.webapp.templates.get_template('modules/zwave/web/remove_node.html')
            root_breadcrumb(webinterface, request)
            webinterface.add_breadcrumb(request, "/module_settings/zwave/remove_node", "Remove Node")
            return page.render(alerts=webinterface.get_alerts())

        @webapp.route("/zwave/cancel_command", methods=['GET'])
        @require_auth()
        def page_tools_module_zwave_cancel_command_get(webinterface, request, session):
            zwave = webinterface._Modules['ZWave']

            zwave.cancel_command()
            page = webinterface.webapp.templates.get_template('modules/zwave/web/cancel_command.html')
            root_breadcrumb(webinterface, request)
            webinterface.add_breadcrumb(request, "/module_settings/zwave/cancel_command", "Cancel Command")
            return page.render(alerts=webinterface.get_alerts())

        @webapp.route("/zwave/soft_reset", methods=['GET'])
        @require_auth()
        def page_tools_module_zwave_soft_reset_get(webinterface, request, session):
            zwave = webinterface._Modules['ZWave']

            zwave.add_node()
            page = webinterface.webapp.templates.get_template('modules/zwave/web/soft_reset.html')
            root_breadcrumb(webinterface, request)
            webinterface.add_breadcrumb(request, "/module_settings/zwave/soft_reset", "Soft Reset")
            return page.render(alerts=webinterface.get_alerts())

        @webapp.route("/zwave/test_network", methods=['GET'])
        @require_auth()
        def page_tools_module_zwave_test_network_get(webinterface, request, session):
            zwave = webinterface._Modules['ZWave']

            zwave.add_node()
            page = webinterface.webapp.templates.get_template('modules/zwave/web/test_network.html')
            root_breadcrumb(webinterface, request)
            webinterface.add_breadcrumb(request, "/module_settings/zwave/test_network", "Test Network")
            return page.render(alerts=webinterface.get_alerts())

        @webapp.route("/zwave/stop_network", methods=['GET'])
        @require_auth()
        def page_tools_module_zwave_soft_reset_get(webinterface, request, session):
            zwave = webinterface._Modules['ZWave']

            zwave.add_node()
            page = webinterface.webapp.templates.get_template('modules/zwave/web/stop_network.html')
            root_breadcrumb(webinterface, request)
            webinterface.add_breadcrumb(request, "/module_settings/zwave/stop_network", "Stop Network")
            return page.render(alerts=webinterface.get_alerts())

        @webapp.route("/zwave/rename_node", methods=['GET'])
        @require_auth()
        def page_tools_module_zwave_rename_node_get(webinterface, request, session):
            zwave = webinterface._Modules['ZWave']

            zwave.add_node()
            page = webinterface.webapp.templates.get_template('modules/zwave/web/rename_node.html')
            root_breadcrumb(webinterface, request)
            webinterface.add_breadcrumb(request, "/module_settings/zwave/rename_node", "Rename Node")
            return page.render(alerts=webinterface.get_alerts())
