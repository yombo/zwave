#!/usr/bin/env bash

#echo "Setup workspace environment"
#cd /usr/local/src
#sudo mkdir yombo
#sudo chown $USER:$USER yombo
#cd /usr/local/src/yombo
#sudo rm -rf /usr/local/src/yombo/*
#
#pip3 install --upgrade cython python_openzwave

echo "Handle python openzave"
cd /usr/local/src/yombo
git clone https://github.com/OpenZWave/python-openzwave.git
cd python-openzwave
#make build
make install &


echo "Handle python libmicrohttpd"
cd /usr/local/src/yombo
wget ftp://ftp.gnu.org/gnu/libmicrohttpd/libmicrohttpd-0.9.55.tar.gz &
wait
tar zxvf libmicrohttpd-0.9.55.tar.gz
cd /usr/local/src/yombo/libmicrohttpd-0.9.55
./configure
make
sudo make install

echo "Handle python open zwave control panel"
cd /usr/local/src/yombo
git clone https://github.com/OpenZWave/open-zwave-control-panel.git
cd /usr/local/src/yombo/open-zwave-control-panel
rm Makefile
cp $ZWAVE_MODULE_PATH/files/ozwcp-Makefile Makefile
make
ln -sd ~/.pyenv/versions/$PYENV_VERSION/lib/python3.6/site-packages/python_openzwave/ozw_config config

