from . import Zwave_Node
from .. import const

# from yombo.lib.devices.light import Light
from yombo.constants.devicetypes.light import ATR_RGB_COLOR, ATR_COLOR_TEMP
from yombo.constants.status_extra import (STATUS_EXTRA_BRIGHTNESS, STATUS_EXTRA_RGB_COLOR, STATUS_EXTRA_LEVEL)
from yombo.utils import is_one_zero
import yombo.utils.color as color_util
from yombo.utils.color import color_rgb_to_rgbw, ct_to_hs

from yombo.core.log import get_logger

logger = get_logger("modules.zwave.devicetypes.lights")

AEOTEC = 0x86
AEOTEC_ZW098_LED_BULB = 0x62
AEOTEC_ZW098_LED_BULB_LIGHT = (AEOTEC, AEOTEC_ZW098_LED_BULB)

COLOR_CHANNEL_WARM_WHITE = 0x01
COLOR_CHANNEL_COLD_WHITE = 0x02
COLOR_CHANNEL_RED = 0x04
COLOR_CHANNEL_GREEN = 0x08
COLOR_CHANNEL_BLUE = 0x10

# Generate midpoint color temperatures for bulbs that have limited
# support for white light colors
TEMP_COLOR_MAX = 500  # mireds (inverted)
TEMP_COLOR_MIN = 154
TEMP_MID_YOMBO = (TEMP_COLOR_MAX - TEMP_COLOR_MIN) / 2 + TEMP_COLOR_MIN
TEMP_WARM_YOMBO = (TEMP_COLOR_MAX - TEMP_COLOR_MIN) / 3 * 2 + TEMP_COLOR_MIN
TEMP_COLD_YOMBO = (TEMP_COLOR_MAX - TEMP_COLOR_MIN) / 3 + TEMP_COLOR_MIN

WORKAROUND_ZW098 = 'zw098'

DEVICE_MAPPINGS = {
    AEOTEC_ZW098_LED_BULB_LIGHT: WORKAROUND_ZW098
}


class Zwave_Dimmer(Zwave_Node):
    """Representation of a Z-Wave dimmer."""

    FRIENDLY_LABEL = "Zwave Dimmer"

    def __init__(self, parent, network, node, schema, primary_value):
        """
        Initialize the light.
        """
        Zwave_Node.__init__(self, parent, network, node, schema, primary_value)
        self.device_type = self._Parent._DeviceTypes.get('zwave_dimmer')
        self.commands = {
            'on': self.turn_on,
            'set_brightness': self.turn_on,
            'set_percent': self.turn_on,
            'off': self.turn_off,
        }

        self.VALUE_LABEL_TO_STATUS_EXTRA.update({
            STATUS_EXTRA_LEVEL: "Level",
        })

        self._zw098 = None

        # Enable appropriate workaround flags for our device
        # Make sure that we have values for the key before converting to int
        if (self.node.manufacturer_id.strip() and
                self.node.product_id.strip()):
            specific_sensor_key = (int(self.node.manufacturer_id, 16),
                                   int(self.node.product_id, 16))
            if specific_sensor_key in DEVICE_MAPPINGS:
                if DEVICE_MAPPINGS[specific_sensor_key] == WORKAROUND_ZW098:
                    logger.debug("AEOTEC ZW098 workaround enabled")
                    self._zw098 = 1

    def _set_duration(self, **kwargs):
        """Set the transition time for the brightness value.
        Zwave Dimming Duration values:
        0x00      = instant
        0x01-0x7F = 1 second to 127 seconds
        0x80-0xFE = 1 minute to 127 minutes
        0xFF      = factory default
        """
        if self.values['dimming_duration'] is None:
            if 'transition' in kwargs:
                logger.debug("Dimming not supported.")
            return

        if 'transition' not in kwargs:
            self.values['dimming_duration'].data = 0xFF
            return

        transition = kwargs['transition']
        if transition <= 127:
            self.values['dimming_duration'].data = int(transition)
        elif transition > 7620:
            self.values['dimming_duration'].data = 0xFE
            logger.warn("Transition clipped to 127 minutes for %s.",
                         self.entity_id)
        else:
            minutes = int(transition / 60)
            logger.debug("Transition rounded to %d minutes for %s.",
                          minutes, self.entity_id)
            self.values['dimming_duration'].data = minutes + 0x7F

    def update_status(self, status_extra_label, value, last_device_command):
        print("UPDATE STATUS!!!!!!!!!!!!!!!!!!!!!!!!! Zwave_Dimmer: %s" % value)

        command = None
        status = None
        status_extra = {}
        if status_extra_label == STATUS_EXTRA_LEVEL:
            new_value = int(value.data)
            if last_device_command is None:
                if new_value == 0:
                    command = 'off'
                elif new_value > 0 and new_value < 99:
                    command = 'set_brightness'
                else:
                    command = 'on'
                command = self._Parent._Commands[command]
            else:
                command = last_device_command.command

            status = is_one_zero(new_value)
        if status_extra_label in self.VALUE_LABEL_TO_STATUS_EXTRA:
            status_extra[status_extra_label] = value.data

        self.set_status(status, status_extra, last_command=command, last_device_command=last_device_command)

    def turn_on(self, **kwargs):
        """Turn the device on."""
        # print("zwave devicetype light:turn_on: %s" % kwargs)
        # self._set_duration(**kwargs)

        # Zwave multilevel switches use a range of [0, 99] to control
        # brightness. Level 255 means to set it to previous value.
        inputs = kwargs['inputs']
        print("zwave light inputs: %s" % inputs)
        if 'brightness' in inputs:
            brightness = int(inputs['brightness'])
        elif 'percent' in inputs:
            brightness = int(inputs['percent'])
        else:
            brightness = 255

        print("zwave brightness: %s" % brightness)
        if self.node.set_dimmer(self.primary_value.value_id, brightness):
            self._state = 'on'
        return 'done', "Command completed."

    def turn_off(self, **kwargs):
        """Turn the device off."""
        # self._set_duration(**kwargs)

        if self.node.set_dimmer(self.primary_value.value_id, 0):
            self._state = 'off'
        return 'done', "Command completed."


class Zwave_ColorLight(Zwave_Dimmer):
    """Representation of a Z-Wave color changing light."""

    FRIENDLY_LABEL = "Zwave Color Light"

    def __init__(self, parent, network, node, schema, primary_value):
        """Initialize the light."""
        Zwave_Dimmer.__init__(self, parent, network, node, schema, primary_value)
        self.device_type = self._Parent._DeviceTypes.get('zwave_colorlight')
        self._color_channels = None
        self._rgb = None
        self._ct = None
        self._white = None
        self._zw098 = None

    def update_status(self, status_extra_label, value, last_device_command):
        super().update_status(status_extra_label, value, last_device_command)
        print("UPDATE STATUS!!!!!!!!!!!!!!!!!!!!!!!!! Zwave_ColorLight: %s" % value)

        values = self.values_by_label
        if values['Color'] is None:
            return
        if values['Color Channels'] is None:
            return

        status_extra = {}

        # Color Channels
        self._color_channels = values['Color Channels'].data

        # Color Data String
        color = values['Color'].data

        # RGB is always present in the openzwave color data string.
        self._rgb = [
            int(color[1:3], 16),
            int(color[3:5], 16),
            int(color[5:7], 16)]
        status_extra[STATUS_EXTRA_RGB_COLOR] = self._rgb

        if status_extra_label in self.VALUE_LABEL_TO_STATUS_EXTRA:
            status_extra[status_extra_label] = value.data

        self.set_status(None, status_extra, last_device_command=last_device_command)


        # # self._hs = color_util.color_RGB_to_hs(*rgb)
        #
        # # Parse remaining color channels. Openzwave appends white channels
        # # that are present.
        # index = 7
        #
        # # Warm white
        # if color_channels & COLOR_CHANNEL_WARM_WHITE:
        #     warm_white = int(data[index:index+2], 16)
        #     index += 2
        # else:
        #     warm_white = 0
        #
        # # Cold white
        # if color_channels & COLOR_CHANNEL_COLD_WHITE:
        #     cold_white = int(data[index:index+2], 16)
        # else:
        #     cold_white = 0
        #
        # Color temperature. With the AEOTEC ZW098 bulb, only two color
        # temperatures are supported. The warm and cold channel values
        # indicate brightness for warm/cold color temperature.
        # if self._zw098:
        #     if warm_white > 0:
        #         self._ct = TEMP_WARM_YOMBO
        #         self._hs = ct_to_hs(self._ct)
        #     elif cold_white > 0:
        #         self._ct = TEMP_COLD_YOMBO
        #         self._hs = ct_to_hs(self._ct)
        #     else:
        #         # RGB color is being used. Just report midpoint.
        #         self._ct = TEMP_MID_YOMBO
        #
        # elif self._color_channels & COLOR_CHANNEL_WARM_WHITE:
        #     self._white = warm_white
        #
        # elif self._color_channels & COLOR_CHANNEL_COLD_WHITE:
        #     self._white = cold_white

        # If no rgb channels supported, report None.
        # if not (self._color_channels & COLOR_CHANNEL_RED or
        #         self._color_channels & COLOR_CHANNEL_GREEN or
        #         self._color_channels & COLOR_CHANNEL_BLUE):
        #     self._hs = None
        #
        # # map to yombo status...
        # self.status_extra['hs'] = self._hs
        # self.status_extra['rgb'] = self._rgb
        # self.status_extra['color_temp'] = self._ct
    #
    # @property
    # def white_value(self):
    #     """Return the white value of this light between 0..255."""
    #     return self._white
    #
    # @property
    # def color_temp(self):
    #     """Return the color temperature."""
    #     return self._ct

    def turn_on(self, **kwargs):
        """Turn the device on."""
        zwave_rgbw = None
        inputs = kwargs['inputs']
        if ATR_COLOR_TEMP in inputs:
            # Color temperature. With the AEOTEC ZW098 bulb, only two color
            # temperatures are supported. The warm and cold channel values
            # indicate brightness for warm/cold color temperature.
            if self._zw098:
                if inputs[ATR_COLOR_TEMP] > TEMP_MID_YOMBO:
                    self._ct = TEMP_WARM_YOMBO
                    rgbw = '#000000ff00'
                else:
                    self._ct = TEMP_COLD_YOMBO
                    rgbw = '#00000000ff'

        elif ATR_RGB_COLOR in inputs:
            input_rgb = inputs[ATR_RGB_COLOR]
            zwave_rgbw = '#'
            for color_value in input_rgb:
                zwave_rgbw += format(int(round(color_value)), '02x')

            # print("zwave values: %s" % self.values['color'])
            zwave_rgbw += '0000'

            # if (not self._zw098 and (
            #         self._color_channels & COLOR_CHANNEL_WARM_WHITE or
            #         self._color_channels & COLOR_CHANNEL_COLD_WHITE)):
            #     rgbw = '#'
            #     for colorval in color_rgb_to_rgbw(*self._rgb):
            #         rgbw += format(colorval, '02x')
            #     rgbw += '00'
            # else:
            #     rgbw = '#'
            #     for colorval in self._rgb:
            #         rgbw += format(colorval, '02x')
            #     rgbw += '0000'

        try:
            value = self.get_value_by_label('Color')
            if zwave_rgbw:
                value.data = zwave_rgbw
        except KeyError as e:
            logger.warn("Cannot setup zwave device. Perhaps the wrong device type: {e}", e=e)
        super().turn_on(**kwargs)
