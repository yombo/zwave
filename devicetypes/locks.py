from yombo.constants.features import FEATURE_AUTO_RELOCK
from yombo.constants.status_extra import (STATUS_EXTRA_AUTO_RELOCK, STATUS_EXTRA_BATTERY_LEVEL,
    STATUS_EXTRA_RELOCK_TIME, STATUS_EXTRA_LOCKED)
from yombo.core.log import get_logger
from yombo.utils import is_one_zero

from . import Zwave_Node

logger = get_logger("modules.zwave.devicetypes.locks")

ATTR_NOTIFICATION = 'notification'
ATTR_LOCK_STATUS = 'lock_status'
CONFIG_ADVANCED = 'Advanced'

POLYCONTROL = 0x10E
DANALOCK_V2_BTZE = 0x2
POLYCONTROL_DANALOCK_V2_BTZE_LOCK = (POLYCONTROL, DANALOCK_V2_BTZE)
WORKAROUND_V2BTZE = 'v2btze'

DEVICE_MAPPINGS = {
    POLYCONTROL_DANALOCK_V2_BTZE_LOCK: WORKAROUND_V2BTZE
}

LOCK_NOTIFICATION = {
    '1': 'Manual Lock',
    '2': 'Manual Unlock',
    '3': 'RF Lock',
    '4': 'RF Unlock',
    '5': 'Keypad Lock',
    '6': 'Keypad Unlock',
    '11': 'Lock Jammed',
    '254': 'Unknown Event'
}

LOCK_ALARM_TYPE = {
    '9': 'Deadbolt Jammed',
    '16': 'Unlocked by Bluetooth ',
    '18': 'Locked with Keypad by user ',
    '19': 'Unlocked with Keypad by user ',
    '21': 'Manually Locked ',
    '22': 'Manually Unlocked ',
    '24': 'Locked by RF',
    '25': 'Unlocked by RF',
    '27': 'Auto re-lock',
    '33': 'User deleted: ',
    '112': 'Master code changed or User added: ',
    '113': 'Duplicate Pin-code: ',
    '130': 'RF module, power restored',
    '144': 'Unlocked by NFC Tag or Card by user ',
    '161': 'Tamper Alarm: ',
    '167': 'Low Battery',
    '168': 'Critical Battery Level',
    '169': 'Battery too low to operate'
}

MANUAL_LOCK_ALARM_LEVEL = {
    '1': 'by Key Cylinder or Inside thumb turn',
    '2': 'by Touch function (lock and leave)'
}

TAMPER_ALARM_LEVEL = {
    '1': 'Too many keypresses',
    '2': 'Cover removed'
}

LOCK_STATUS = {
    '1': True,
    '2': False,
    '3': True,
    '4': False,
    '5': True,
    '6': False,
    '9': False,
    '18': True,
    '19': False,
    '21': True,
    '22': False,
    '24': True,
    '25': False,
    '27': True
}

ALARM_TYPE_STD = [
    '18',
    '19',
    '33',
    '112',
    '113',
    '144'
]



class Zwave_Lock(Zwave_Node):
    """Representation of a Z-Wave Lock."""

    FRIENDLY_LABEL = "Zwave Lock"

    def __init__(self, parent, network, node, schema, primary_value):
        """Initialize the lock."""
        Zwave_Node.__init__(self, parent, network, node, schema, primary_value)
        self.VALUE_LABEL_TO_STATUS_EXTRA.update({
            STATUS_EXTRA_AUTO_RELOCK: "Auto Relock",
            STATUS_EXTRA_RELOCK_TIME: "Re-lock Time",
            STATUS_EXTRA_LOCKED: "Locked"
        })
        self.commands = {
            'lock': self.lock,
            'unlock': self.unlock,
        }
        self.device_type = self._Parent._DeviceTypes.get('zwave_lock')
        self._v2btze = None

        # Enable appropriate workaround flags for our device
        # Make sure that we have values for the key before converting to int
        if (self.node.manufacturer_id.strip() and
                self.node.product_id.strip()):
            specific_sensor_key = (int(self.node.manufacturer_id, 16),
                                   int(self.node.product_id, 16))
            if specific_sensor_key in DEVICE_MAPPINGS:
                if DEVICE_MAPPINGS[specific_sensor_key] == WORKAROUND_V2BTZE:
                    self._v2btze = 1
                    logger.debug("Polycontrol Danalock v2 BTZE workaround enabled")

    def update_status(self, status_extra_label, value, last_device_command):
        battery_level = self.node.get_battery_level()
        if battery_level is None:
            delay = 0.100
        else:
            delay = 0.300

        status = None
        status_extra = {}
        if status_extra_label == STATUS_EXTRA_LOCKED:
            status = is_one_zero(value.data)
        if status_extra_label in (STATUS_EXTRA_LOCKED, STATUS_EXTRA_BATTERY_LEVEL):
            status_extra[status_extra_label] = value.data

        self.set_status(status, status_extra, last_device_command=last_device_command)


        # if 'access_control' in self.values:
        #     notification_data = self.values['access_control'].data
        #     self._notification = LOCK_NOTIFICATION.get(str(notification_data))
        #
        #     if self._v2btze:
        #         if 'v2btze_advanced' in self.values and \
        #                 self.values['v2btze_advanced'].data == CONFIG_ADVANCED:
        #             self._lock_status = LOCK_STATUS.get(str(notification_data))
        #             logger.debug(
        #                 "Lock state set from Access Control value and is %s, "
        #                 "get=%s", str(notification_data), self._lock_status)
        #
        # if 'alarm_type' not in self.values:
        #     return
        #
        # # print("lock values: %s" % self.values)
        # alarm_type = self.values['alarm_type.data']
        # logger.info("Lock alarm_type is {alarm_type}", alarm_type=str(alarm_type))
        # if 'alarm_level' in self.values:
        #     alarm_level = self.values['alarm_level'].data
        # else:
        #     alarm_level = None
        # logger.info("Lock alarm_level is {alarm_level}", alarm_level=str(alarm_level))
        #
        # if not alarm_type:
        #     return
        # if alarm_type == 21:
        #     self._lock_status = '{}{}'.format(
        #         LOCK_ALARM_TYPE.get(str(alarm_type)),
        #         MANUAL_LOCK_ALARM_LEVEL.get(str(alarm_level)))
        #     return
        # if str(alarm_type) in ALARM_TYPE_STD:
        #     self._lock_status = '{}{}'.format(
        #         LOCK_ALARM_TYPE.get(str(alarm_type)), str(alarm_level))
        #     return
        # if alarm_type == 161:
        #     self._lock_status = '{}{}'.format(
        #         LOCK_ALARM_TYPE.get(str(alarm_type)),
        #         TAMPER_ALARM_LEVEL.get(str(alarm_level)))
        #     return
        # if alarm_type != 0:
        #     self._lock_status = LOCK_ALARM_TYPE.get(str(alarm_type))
        #     return

    @property
    def is_locked(self):
        """Return true if device is locked."""
        return self.primary_value.data

    def lock(self, **kwargs):
        """Lock the device."""
        self.primary_value.data = True

    def unlock(self, **kwargs):
        """Unlock the device."""
        self.primary_value.data = False
