#This file was created by Yombo for use with Yombo Gateway automation
#software. Details can be found at https://yombo.net
"""
ZWave Support
=============

Provides ZWave support Yombo Gateway using Open Zwave, with Python-Openzwave
wrapper.

License
=======

See LICENSE.md for full license and attribution information.

The Yombo team and other contributors hopes that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
more details.

.. moduleauthor:: Mitch Schwenk <mitch-gw@yombo.net>
:license: Apache 2.0
"""
# Import python libraries
from collections import OrderedDict
from pydispatch import dispatcher
try:  # Prefer simplejson if installed, otherwise json will work swell.
    import simplejson as json
except ImportError:
    import json
import os
from os.path import dirname, abspath
import traceback

try:
    import openzwave
    from openzwave.group import ZWaveGroup
    from openzwave.node import ZWaveNode
    from openzwave.value import ZWaveValue
    from openzwave.scene import ZWaveScene
    from openzwave.controller import ZWaveController
    from openzwave.network import ZWaveNetwork
    from openzwave.option import ZWaveOption
    from openzwave.object import ZWaveException
    HAS_OPENZAVE = True
    import python_openzwave
    ZWAVE_PATH = os.path.dirname(os.path.dirname(python_openzwave.__file__)) + "/python_openzwave"
except ImportError:
    HAS_OPENZAVE = False
    ZWAVE_PATH = None

from twisted.internet.defer import inlineCallbacks
from twisted.internet import reactor

from yombo.utils import sleep, sha256_compact, random_string
from yombo.utils.decorators import memoize_ttl
from yombo.core.log import get_logger
from yombo.core.module import YomboModule

from . import const as zconstants
from .discovery_schemas import DISCOVERY_SCHEMAS
from .util import check_node_schema, check_value_schema, node_name
from .devicetypes.fans import Zwave_Fan
from .devicetypes.lights import Zwave_Dimmer, Zwave_ColorLight
from .devicetypes.locks import Zwave_Lock
from .devicetypes.sensors import ZWave_Sensor, ZWave_Multilevel_Sensor, ZWave_Alarm_Sensor
from .devicetypes.switches import Zwave_Switch
from . import workaround

from yombo.modules.zwave.web_routes import module_zwave_routes

logger = get_logger("modules.zwave")


class ZWave(YomboModule):
    """
    Adds support for ZWave devices.
    """
    def _obj_to_dict(self, obj):
        """
        Convert an object into a hash for debug.
        """
        return {key: getattr(obj, key) for key
                in dir(obj)
                if key[0] != '_' and not hasattr(getattr(obj, key), '__call__')}

    @inlineCallbacks
    def _init_(self, **kwargs):
        self.nodes = {}  # multidemensional dict. First is home_id, node_ids inside
        self.device_command_queue = OrderedDict()
        self.zwave_devices = yield self._SQLDict.get(self, "zwave_devices")  # tracks history of devices found
        self.working_dir = self._Atoms['working_dir']
        self.full_path = dirname(abspath(__file__))
        self.enabled = True
        self.network_available = False

        if HAS_OPENZAVE is False:
            logger.warn("ZWave support disabled. Requirements not met. See: %s" % self.full_path + "/readme.rst")
            self.enabled = False
            return

        self._module_starting()
        if not os.path.exists("%s/etc/openzwave" % self.working_dir):
            os.makedirs("%s/etc/openzwave" % self.working_dir)

        # self.interface_port = self._module_variables_cached['port']['values'][0]
        self.interface_port = '/dev/zwave'
        self.generate_options_file()

    def _load_(self, **kwargs):
        if self.enabled is False:
            return
        try:
            logger.debug("about to connect to zwave...: {port}", port=self.interface_port)
            self.zwave_options = ZWaveOption(self.interface_port,
                                             config_path="%s/ozw_config" % ZWAVE_PATH,
                                             user_path="%s/etc/openzwave" % self.working_dir
                                             )
        except ZWaveException as e:
            logger.error("Error connecting to zwave: {reason}",
                         reason="%s...  Truncated %s characters." % (str(e)[:150], len(str(e)[150:])) )
            logger.error("Disabling ZWave module.")
            self.enabled = False
            return
        self.zwave_options.set_log_file("%s/log/openzwave.log" % self.working_dir)
        self.zwave_options.set_append_log_file(False)
        self.zwave_options.set_console_output(False)
        self.zwave_options.set_logging(True)
        self.zwave_options.set_save_log_level("Info")
        self.zwave_options.lock()

        # Create a network object
        self.zwavenetwork = ZWaveNetwork(self.zwave_options, autostart=False)

        # We connect to the dispatcher
        dispatcher.connect(self.z_network_ready, ZWaveNetwork.SIGNAL_NETWORK_READY)
        dispatcher.connect(self.z_network_failed, ZWaveNetwork.SIGNAL_NETWORK_FAILED)
        dispatcher.connect(self.z_network_stopped, ZWaveNetwork.SIGNAL_NETWORK_STOPPED)
        dispatcher.connect(self.z_network_awaked, ZWaveNetwork.SIGNAL_NETWORK_AWAKED)
        dispatcher.connect(self.z_node_event, ZWaveNetwork.SIGNAL_NODE_EVENT)
        dispatcher.connect(self.z_node_ready, ZWaveNetwork.SIGNAL_NODE_READY)
        dispatcher.connect(self.z_node_removed, ZWaveNetwork.SIGNAL_NODE_REMOVED)
        dispatcher.connect(self.zscene_event, ZWaveNetwork.SIGNAL_SCENE_EVENT)
        dispatcher.connect(self.zvalue_refreshed, ZWaveNetwork.SIGNAL_VALUE_REFRESHED)
        dispatcher.connect(self.zvalue_removed, ZWaveNetwork.SIGNAL_VALUE_REMOVED)
        dispatcher.connect(self.zbutton_create, ZWaveNetwork.SIGNAL_CREATE_BUTTON)
        dispatcher.connect(self.zbutton_delete, ZWaveNetwork.SIGNAL_DELETE_BUTTON)
        dispatcher.connect(self.zbutton_on, ZWaveNetwork.SIGNAL_BUTTON_ON)
        dispatcher.connect(self.zbutton_off, ZWaveNetwork.SIGNAL_BUTTON_OFF)
        dispatcher.connect(self.zvalue, ZWaveNetwork.SIGNAL_VALUE)


        # dispatcher.connect(self.z_network_ressetted, ZWaveNetwork.SIGNAL_NETWORK_RESETTED)
        ## dispatcher.connect(self.z_node_new, ZWaveNetwork.SIGNAL_NODE_NEW)  # new node with more details.
        # dispatcher.connect(self.z_node_naming, ZWaveNetwork.SIGNAL_NODE_NAMING)
        ## dispatcher.connect(self.z_node_protocol_info, ZWaveNetwork.SIGNAL_NODE_PROTOCOL_INFO)
        # dispatcher.connect(self.zvalue_added, ZWaveNetwork.SIGNAL_VALUE_ADDED)
        # dispatcher.connect(self.zvalue_changed, ZWaveNetwork.SIGNAL_VALUE_CHANGED)
        # dispatcher.connect(self.znode, ZWaveNetwork.SIGNAL_NODE)
        # dispatcher.connect(self.znotification, ZWaveNetwork.SIGNAL_NOTIFICATION)

        ## dispatcher.connect(self.z_essential_node_queries_complete,
        ##                    ZWaveNetwork.SIGNAL_ESSENTIAL_NODE_QUERIES_COMPLETE)
        ## dispatcher.connect(self.z_node_queries_complete, ZWaveNetwork.SIGNAL_NODE_QUERIES_COMPLETE)
        dispatcher.connect(self.z_awake_nodes_queried, ZWaveNetwork.SIGNAL_AWAKE_NODES_QUERIED)

        self.zwavenetwork.start()

    def _stop_(self, **kwargs):
        if self.enabled is False:
            return
        self.zwavenetwork.stop()

    def generate_options_file(self):
        def generate_network_key():
            results = []
            for x in range(0, 16):
                results.append("0x%s" % random_string(length=2, letters="ABCDEF0123456789"))
            return ", ".join(results)

        logging = self._Configs.get("zwave", "logging", "true", False)
        associate = self._Configs.get("zwave", "Associate", "true", False)
        notifytransaction = self._Configs.get("zwave", "NotifyTransactions", "false", False)
        drivermaxattempts = self._Configs.get("zwave", "DriverMaxAttempts", "5", False)
        saveconfiguration = self._Configs.get("zwave", "SaveConfiguration", "true", False)
        networkkey = self._Configs.get("zwave", "NetworkKey", generate_network_key())
        refreshallusercodes = self._Configs.get("zwave", "RefreshAllUserCodes", "false", False)
        threadterminatetimeout = self._Configs.get("zwave", "ThreadTerminateTimeout", "5000", False)

        config_file = open("%s/etc/openzwave/options.xml" % self.working_dir, 'w')
        config_file.write('<?xml version="1.0" encoding="utf-8"?>\n')
        config_file.write('<!-- This file is managed by Yombo Zwave module. Any changes will be lost. -->\n')
        config_file.write("<Options xmlns='http://code.google.com/p/open-zwave/'>\n")
        config_file.write('  <Option name="logging" value="%s" />\n' % logging)
        config_file.write('  <Option name="Associate" value="%s" />\n' % associate)
        config_file.write('  <Option name="NotifyTransactions" value="%s" />\n' % notifytransaction)
        config_file.write('  <Option name="DriverMaxAttempts" value="%s" />\n' % drivermaxattempts)
        config_file.write('  <Option name="SaveConfiguration" value="%s" />\n' % saveconfiguration)
        config_file.write('  <Option name="NetworkKey" value="%s" />\n' % networkkey)
        config_file.write('  <Option name="RefreshAllUserCodes" value="%s" />\n' % refreshallusercodes)
        config_file.write('  <Option name="ThreadTerminateTimeout" value="%s" />\n' % threadterminatetimeout)
        config_file.write('</Options>\n')
        config_file.write('\n')
        config_file.close()

    def z_network_awaked(self, network):
        logger.info("Hello from network : z_network_awaked : {nodes} nodes were found.", nodes=network.nodes_count)

    def z_network_ready(self, network):
        # logger.info("Hello from network : z_network_ready : {nodes} nodes were found.", nodes=network.nodes_count)
        reactor.callLater(0.1, self.finish_startup, network)

    @inlineCallbacks
    def finish_startup(self, network):
        if self.network_available is True:
            return
        self.network_available = True
        dispatcher.connect(self.z_node_added,
                           ZWaveNetwork.SIGNAL_NODE_ADDED)  # a new node, no details are known yet.
        network.set_poll_interval(2000, True)

        if network.home_id not in self.nodes:
            self.nodes[network.home_id] = {}

        for node_id, node in network.nodes.items():
            if node.node_id not in self.nodes[network.home_id]:
                yield self.node_added(network, node)

        self.network_available = True

        for k in list(self.device_command_queue.keys()):
            # print("zwave processing queue....")
            kwargs = self.device_command_queue[k]
            # print("sending delayed command: %s" % kwargs)
            self._device_command_(**kwargs)
            del self.device_command_queue[k]
            yield sleep(0.150)
        self._module_started()

    ########
    # Z-Wave tools/functions.
    ########

    def add_node(self):
        """
        Tells the controller to add a new node.
        """
        logger.info("Z-Wave add_node started.")
        return self.zwavenetwork.controller.add_node()

    def add_node_secure(self):
        """
        Tells the controller to add a new SECURE node.
        """
        logger.info("Z-Wave add_node (secure) started.")
        return self.zwavenetwork.controller.add_node(True)

    def remove_node(self):
        """
        Tells the controller to remove a node.
        """
        logger.info("Z-Wave remove_node started.")
        return self.zwavenetwork.controller.remove_node()

    def cancel_command(self):
        """
        Cancels any pending commands, such as adding or removing a node.
        """
        logger.info("Cancelling Z-Wave command.")
        return self.zwavenetwork.controller.cancel_command()

    def soft_reset(self):
        """
        Soft resets the Z-Wave controller.
        """
        logger.info("Soft resetting Z-Wave controller.")
        return self.zwavenetwork.controller.soft_reset(self.zwavenetwork.home_id)

    def test_network(self):
        """
        Test Z-Wave network by trying to send commands to all nodes.
        """
        logger.info("Soft resetting Z-Wave controller.")
        return self.zwavenetwork.controller.test()

    def stop_network(self):
        """
        Stop Z-Wave network.
        """
        logger.info("Stopping Z-Wave network")
        self.zwavenetwork.controller.stop()

    def rename_node(self, home_id, node_id, name):
        """
        Rename a node.
        """
        node = self.nodes[home_id][node_id].node
        node.name = name
        logger.info("Z-Wave node renamed node id {node_id} to {name}",
                    node_id=node_id, name=name)

    def heal(self):
        """
        Heal network by requesting nodes rediscover their neighbors.
        Sends a ControllerCommand_RequestNodeNeighborUpdate to every node.
        Can take a while on larger networks.

        :return:
        """
        logger.info("Starting zwave network heal.")
        return self.zwavenetwork.heal()

    def get_scenes(self):
        """
        The scenes of the network.
        Scenes are generated directly from the lib. There is no notification
        support to keep them up to date. So for a batch job, consider
        storing them in a local variable.
        :return: return a dict() (that can be empty) of scene object. Return None if betwork is not ready
        :rtype: dict() or None
        """
        return self.zwavenetwork.get_scenes()

    def z_network_failed(self, network):
        logger.debug("Hello from network : failed.  {network}", network=network)
        self.network_available = False

    def z_network_stopped(self, network):
        logger.debug("Hello from network : z_network_stopped")
        self.network_available = False

    # def z_network_ressetted(self, network):
    #     logger.debug("Hello from network : z_network_ressetted")

    def z_node_added(self, network, node):  # node already exists in controller
        logger.info("############## Hello from network : z_node_added: {node}", node=node)
        # self.node_added(network, node)

    def z_node_event(self, network, node):
        print("Hello from node : z_node_event {}.".format(node))

    # def z_node_naming(self, network, node):
    #     print("Hello from node : z_node_naming {}.".format(node))

    def z_node_protocol_info(self, network, node):
        print("Hello from node : z_node_protocol_info {}.".format(node))

    def z_essential_node_queries_complete(self, network):
        print("######################### Hello from node : z_essential_node_queries_complete")

    def z_node_ready(self, network, node):
        print("Hello from node : z_node_ready {}.".format(node))

    def z_node_removed(self, network, node):
        print("Hello from node : z_node_removed {}.".format(node))
        self.controller_removed_node(network.home_id, node.node_id)

    # def znode(self, node):
        # print("############ Hello from node : znode {}.".format(node))
        # for home_id, nodes in self.nodes.items():
        #     for node_id, zwavenode in nodes.items():
        #         if zwavenode is None:
        #             self.controller_removed_node(home_id, node_id)

    def controller_removed_node(self, home_id, node_id):
        print("controler removed node start")
        if home_id in self.nodes:
            print("controler removed node start: we have a matching home...")
            if node_id in self.nodes[home_id]:
                print("controler removed node start: we have a matching node...")
                self._Notifications.add({
                    'title': 'Zwave node removed, Yombo Device unusable',
                             'message': 'The Zwave node removed from Zwave network and renders the this device unusable: %s <br>' %
                                        (self.nodes[home_id][node_id].FRIENDLY_LABEL,
                                         ),
                'source': zconstants.DISCOVERY_SOURCE,
                'persist': True,
                'priority': 'high',
                'always_show': True,
                'always_show_allow_clear': True,
                # 'id': 'zwave_removed%s:%s' % (home_id, node_id),
                })
                del self.nodes[home_id][node_id]

            id_hash = self.encode_node_id(home_id, node_id)
            self._Discovery.delete(discover_id=id_hash)

    def zscene_event(self, network, node):
        print("Hello from node : zscene_event {}.".format(node))

    def zvalue_added(self, node, value):
        print("Hello from node : zvalue_added node:{} = {}.".format(node, value))

    def zvalue_changed(self, value):
        print("Hello from node : zvalue_changed {}.".format(value))

    def zvalue_refreshed(self, network, node):
        print("Hello from node : zvalue_refreshed {}.".format(node))

    def zvalue_removed(self, network, node):
        print("Hello from node : zvalue_removed {}.".format(node))

    def zbutton_create(self, network, node):
        print("Hello from node : zbutton_create {}.".format(node))

    def zbutton_delete(self, network, node):
        print("Hello from node : zbutton_delete {}.".format(node))

    def zbutton_on(self, network, node):
        print("Hello from node : zbutton_on {}.".format(node))

    def zbutton_off(self, network, node):
        print("Hello from node : zbutton_off {}.".format(node))

    def z_awake_nodes_queried(self, network, **kwargs):
        print("Hello from node : z_awake_nodes_queried")

    def encode_node_id(self, home_id, node_id):
        return sha256_compact(str("%s::%s" % (home_id, node_id)).encode())

    def decode_node_id_hash(self, hash_id):
        if hash_id in self.zwave_devices:
            return self.zwave_devices[hash_id]

    def node_added(self, network, node):
        logger.debug("node_added starting.. network: {network}, node: {node}", network=network, node=node)
        for value_id, value in node.values.items():
            # print(" - value_id: %s, value: %s" % (value_id, value))
            # print("   - polling. Intensity: %s, is_polled: %s" % (value.poll_intensity, value.is_polled))
            schema = self.get_node_schema(node, value)
            if schema is not None:
                component = schema['component']
                # print("zwave schema: %s" % schema)
                workaround_component = workaround.get_device_component_mapping(value)
                if workaround_component and workaround_component != component:
                    if workaround_component == workaround.WORKAROUND_IGNORE:
                        logger.info("Ignoring Node {node} due to workaround_ignore.", node=node)
                    else:
                        logger.debug("Using component {new} instead of {old}", new=workaround_component, old=component)
                        schema['component'] = workaround_component

                try:
                    self._do_node_added(network, node, schema, value)
                except Exception as e:
                    logger.error("-------==(Error adding new zwave node)==-------------- -")
                    logger.error("{trace}", trace=traceback.format_exc())
                    logger.error("--------------------------------------------------------")
                    logger.warn(
                        "An exception of type {etype} occurred in yombo.lib.nodes:import_component. Message: {msg}",
                        etype=type(e), msg=e)
                    logger.error("--------------------------------------------------------")
                return

    def _do_node_added(self, network, node, schema, primary_value):
        component = schema['component']
        import traceback
        try:
            if component == 'fan':
                self.nodes[network.home_id][node.node_id] = Zwave_Fan(self, network, node, schema, primary_value)

            elif component == 'light':
                if node.has_command_class(zconstants.COMMAND_CLASS_SWITCH_COLOR):
                    logger.debug("Adding zwave color light")
                    self.nodes[network.home_id][node.node_id] = Zwave_ColorLight(self, network, node, schema,
                                                                                 primary_value)
                else:
                    logger.debug("Adding zwave dimmer light")
                    self.nodes[network.home_id][node.node_id] = Zwave_Dimmer(self, network, node, schema, primary_value)

            elif component == 'lock':
                logger.info("Adding zwave lock")
                self.nodes[network.home_id][node.node_id] = Zwave_Lock(self, network, node, schema, primary_value)

            elif component == 'sensor':
                logger.debug("Adding zwave sensor")
                if node.has_command_class(zconstants.COMMAND_CLASS_SENSOR_MULTILEVEL):
                    self.nodes[network.home_id][node.node_id] = ZWave_Multilevel_Sensor(self, network, node, schema,
                                                                                        primary_value)
                if node.has_command_class(zconstants.COMMAND_CLASS_ALARM):
                    self.nodes[network.home_id][node.node_id] = ZWave_Alarm_Sensor(self, network, node, schema,
                                                                                   primary_value)
                else:
                    self.nodes[network.home_id][node.node_id] = ZWave_Sensor(self, network, node, schema, primary_value)

            elif component == 'switch':
                logger.debug("Adding zwave switch")
                self.nodes[network.home_id][node.node_id] = Zwave_Switch(self, network, node, schema, primary_value)

            elif component == 'fan':
                logger.debug("Adding zwave fan")
                self.nodes[network.home_id][node.node_id] = Zwave_Fan(self, network, node, schema, primary_value)

            else:
                logger.warn("Unknown zwave component type: {component}", component=component)
                return
        except Exception as e:
            logger.warn("-==(Warning: Zwave had trouble create Yombo device==-")
            logger.warn("Error: {e}", e=e)
            logger.warn("---------------==(Traceback)==------------------------------")
            logger.warn("{trace}", trace=traceback.format_exc())
            logger.warn("------------------------------------------------------------")
            return

        zwave_device = self.nodes[network.home_id][node.node_id]
        id_hash = self.encode_node_id(network.home_id, node.node_id)

        # if a matching yombo device is found, lets attach to it.
        try:
            yombo_device = self.find_yombo_device(network.home_id, node.node_id)
        except KeyError as e:
            yombo_device = None

        if yombo_device is not None:
            self.nodes[network.home_id][node.node_id].attach_yombo_device(yombo_device)

        if id_hash not in self.zwave_devices:
            self.zwave_devices[id_hash] = {'home_id': network.home_id, 'node_id': node.node_id}

        # Always tell discovery about every device, no matter if it's old or new. It'll determine if it's new
        # based on the device_id we give it.
        logger.info("discovered zwave device: {product_name}, {home_id}:{node_id}",
                    product_name=node.product_name,
                    home_id=network.home_id,
                    node_id=node.node_id)
        self._Discovery.new(discover_id=id_hash, device_data={
            'source': zconstants.DISCOVERY_SOURCE,
            'discover_id': id_hash,
            'description': '%s %s' % (node.manufacturer_name, node.product_name),
            'mfr': node.manufacturer_name,
            'model': node.product_id,
            'serial': "%s:%s" % (network.home_id, node.node_id),
            'label': '',
            'machine_label': '',
            'device_type': zwave_device.device_type,
            'variables': {
                'home_id': str(network.home_id),
                'node_id': str(node.node_id),
                },
            'yombo_device': yombo_device
            }, **{
            'notification_title': 'New Zwave device found',
            'notification_message': 'The Zwave module found a new Zwave device. <p>Type: %s<br>Node Id: %s<br>Manufacturer: %s<br>Product ID: %s<br>' %
                       (node.product_name, node.node_id, node.manufacturer_name, node.product_id),
            }
        )

    @memoize_ttl(120)
    def find_yombo_device(self, home_id, node_id):
        """
        Looks for an zwave device given the provided home id and node id.

        :param requested_address:
        :return: the device pointer
        """
        devices = self._module_devices_cached
        for device_id, device in devices.items():
            device_home_id = device.device_variables_cached['home_id']['values'][0]
            if device_home_id != home_id:
                continue
            device_node_id = device.device_variables_cached['node_id']['values'][0]
            if device_node_id != node_id:
                continue
            return device
        return None

    def get_node_schema(self, node, value):
        # print("get_node_schema: %s, %s" % (node, value))
        for schema in DISCOVERY_SCHEMAS:
            # print("checking schema: %s" % schema)
            if not check_node_schema(node, schema):
                # print("zwaveing schema checking failed 1")
                continue
            if not check_value_schema(
                    value,
                    schema[zconstants.DISC_VALUES][zconstants.DISC_PRIMARY]):
                # print("zwaveing schema checking failed 2")
                continue
            # print("found schema: %s" % schema)
            # value.enable_poll(10)
            return schema

    def zvalue(self, value):
        home_id = value.home_id
        node_id = value.parent_id
        if home_id in self.nodes:
            if node_id in self.nodes[home_id]:
                self.nodes[home_id][node_id].update_value(value)

    def znotification(self, network):
        print("Hello from node : znotification {}.".format(network))

    def _device_imported_(self, **kwargs):
        if self.network_available is False:
            return
        device = kwargs['device']
        if self._is_my_device(device) is False:
            return

        if device.device_type_id not in self._module_device_types_cached:
            print("zwave doesn't care about device: %s" % device.area_label)
            return

        print("device imported...")
        device_variables = device.device_variables_cached

        device_node_id = device_variables['node_id']['values'][0]
        device_home_id = device_variables['home_id']['values'][0]
        if device_home_id not in self.nodes:
            logger.warn("Yombo reports a zwave home_id for a device, but home_id missing. Device: %s" % device.area_label)
            return
        if device_node_id not in self.nodes[device_home_id]:
            logger.warn("Yombo reports a zwave node_id for a device, but node_id missing. Device: %s" % device.area_label)
            return

    # @inlineCallbacks
    def _device_command_(self, **kwargs):
        """
        Received a request to do perform a command for a piface digital output interface.

        :param kwags: Contains 'device' and 'command'.
        :return: None
        """
        device = kwargs['device']
        if self._is_my_device(device) is False:
            return  # not meant for us.
        request_id = kwargs['request_id']

        device_variables = device.device_variables_cached
        device_home_id = device_variables['home_id']['values'][0]
        device_node_id = device_variables['node_id']['values'][0]

        if self.network_available is False:
            addr = self.encode_node_id(device_home_id, device_node_id)
            if addr in self.device_command_queue:
                device.device_command_failed(request_id, message='Command superseded by another.')
            self.device_command_queue[addr] = kwargs
            device.device_command_pending(request_id, message=_('module.zwave', 'ZWave module still starting, command will be processed when fully loaded.'))
            return

        if device_home_id not in self.nodes:
            logger.warn("Yombo reports a zwave home_id for a device, but home_id missing. Device: %s" % device.area_label)
            device.device_command_failed(request_id,
                                         message=_('module.zwave', 'Device is missing valid home_id'))
            return
        if device_node_id not in self.nodes[device_home_id]:
            logger.warn("Yombo reports a zwave node_id for a device, but node_id missing. Device: %s" % device.area_label)
            device.device_command_failed(request_id,
                                         message=_('module.zwave', 'Device is missing valid node_id'))
            return

        zwave_device = self.nodes[device_home_id][device_node_id]

        device.device_command_processing(request_id, message=_('module.zwave', 'Device command being processed by ZWave module.'))

        results = zwave_device.do_command(**kwargs)
        if results[0] == 'failed':
            device.device_command_failed(request_id, message=results[1])
        elif results[0] == 'done':
            device.device_command_done(request_id, message=results[1])
        else:
            device.device_command_done(request_id)

    def _device_deleted_(self, **kwargs):
        device = kwargs['device']
        if self._is_my_device(device) is False:
            return  # not meant for us.
        print("###  a zwave devbice was deleted....lets do stuff")

        device_variables = device.device_variables_cached
        device_home_id = device_variables['home_id']['values'][0]
        device_node_id = device_variables['node_id']['values'][0]

        id_hash = self.encode_node_id(device_home_id, device_node_id)
        try:
            self._Discovery.set_yombo_device(discover_id=id_hash, yombo_device=None, source=zconstants.DISCOVERY_SOURCE)
        except Exception:
            pass

    def _webinterface_add_routes_(self, **kwargs):
        """
        Adds a configuration block to the web interface. This allows users to view their
        zwave devices. and quickly add a new zwave device.

        :param kwargs:
        :return:
        """
        return {
            'nav_side': [
                {
                    'label1': 'Module Settings',
                    'label2': 'Zwave',
                    'priority1': 820,  # Even with a value, 'Tools' is already defined and will be ignored.
                    'priority2': 100,
                    'icon': 'fa fa-cog fa-fw',
                    'url': '/module_settings/zwave/index',
                    'tooltip': '',
                    'opmode': 'run',
                },
            ],
            'routes': [
                module_zwave_routes,
            ],
        }
